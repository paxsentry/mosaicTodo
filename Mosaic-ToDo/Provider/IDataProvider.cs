﻿using System.Collections.Generic;
using Mosaic_ToDo.Models;

namespace Mosaic_ToDo.Provider
{
   /// <summary>
   /// Connection provider between datastore and controller
   /// </summary>
   public interface IDataProvider
   {
      IEnumerable<TodoModel> GetTodos();

      TodoModel UpdateTodoRecord(int id);

      bool DeleteTodo(int id);

      bool CreateTodo(string todo);
   }
}
