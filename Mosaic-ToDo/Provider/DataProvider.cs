﻿using System.Collections.Generic;
using Mosaic_ToDo.Models;
using Mosaic_ToDo.Repository;

namespace Mosaic_ToDo.Provider
{
   public class DataProvider : IDataProvider
   {
      private readonly IDataStore _repository;
      private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

      public DataProvider(IDataStore respository)
      {
         _repository = respository;
      }

      public bool CreateTodo(string todo)
      {
         if (!string.IsNullOrWhiteSpace(todo))
         {
            if (_repository.Insert(todo)) { return true; }
            else
            {
               _logger.Error($"Error during creating Todo with text: '{todo}'");
               return false;
            }
         }

         _logger.Error("Can't create Todo with empty text!");
         return false;
      }

      public bool DeleteTodo(int id)
      {
         if (ValidateId(id))
         {
            if (_repository.Delete(id)) { return true; }
            else
            {
               _logger.Error($"Error during removing Todo with id: '{id}'");
               return false;
            }
         }

         _logger.Error("Unsuccessful delete, reason: invalid Todo id supplied!");
         return false;
      }

      public IEnumerable<TodoModel> GetTodos()
      {
         return _repository.GetAllTodo();
      }

      public TodoModel UpdateTodoRecord(int id)
      {
         if (ValidateId(id))
         {
            return _repository.Update(id);
         }

         _logger.Error("Unsuccessful update, reason: invalid Todo id supplied!");
         return null;
      }

      private bool ValidateId(int id)
      {
         if (id > 0 && id < int.MaxValue) { return true; }

         return false;
      }
   }
}