﻿using System.Linq;
using System.Web.Mvc;
using Mosaic_ToDo.Models;
using Mosaic_ToDo.Provider;
using Mosaic_ToDo.ViewModels;

namespace Mosaic_ToDo.Controllers
{
   public class TodoController : Controller
   {
      private readonly IDataProvider _dataProvider;
      private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

      public TodoController(IDataProvider dataProvider)
      {
         _dataProvider = dataProvider;
      }

      public ActionResult Index()
      {
         TodoViewModel model = new TodoViewModel
         {
            Todos = _dataProvider.GetTodos().ToList()
         };

         return View(model);
      }

      public ActionResult Create(TodoViewModel model)
      {
         if (ModelState.IsValid)
         {
            _logger.Debug($"Creating new Todo with '{model.NewTodoText}' text.");

            if (!_dataProvider.CreateTodo(model.NewTodoText))
            {
               _logger.Error("Error returned from provider during create process, please check other logs.");
            }
         }

         return RedirectToAction("Index");
      }

      public ActionResult Remove(int todoId)
      {
         _logger.Debug($"Delete Todo with id '{todoId}' initiated.");

         if (!_dataProvider.DeleteTodo(todoId))
         {
            _logger.Error("Error returned from provider during delete process, please check other logs.");
         }

         return RedirectToAction("Index");
      }

      public ActionResult Update(int todoId)
      {
         _logger.Debug($"Update Todo with id '{todoId}' initiated.");
         var updatedModel = _dataProvider.UpdateTodoRecord(todoId);

         if (updatedModel == null)
         {
            _logger.Error("Empty record returned from provider during update process, please check other logs.");
         }

         return PartialView("_TodoModel", updatedModel);
      }
   }
}