﻿using System.Collections.Generic;
using System.Linq;
using Mosaic_ToDo.Models;

namespace Mosaic_ToDo.Repository
{
   public sealed class DataStore : IDataStore
   {
      public DataStore() { }

      private static List<TodoModel> dataStorage = null;
      private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

      public static List<TodoModel> DataStorage
      {
         get
         {
            if (dataStorage == null) { dataStorage = GenerateInitialData(); }
            return dataStorage;
         }
      }

      public bool Insert(string todo)
      {
         DataStorage.Add(new TodoModel
         {
            TodoId = dataStorage.Count + 1,
            TodoText = todo,
            Complete = false
         });

         _logger.Debug("New Todo item added to collection");
         return true;
      }

      public bool Delete(int id)
      {
         var dataToRemove = FindById(id);

         if (dataToRemove != null)
         {
            DataStorage.Remove(dataToRemove);
            _logger.Debug($"Todo with id: '{id}' successfully removed from collection.");
            return true;
         }

         _logger.Error($"Can't delete Todo with id'{id}', reason: does not exist!");
         return false;
      }

      public TodoModel Update(int id)
      {
         var itemToUpdate = FindById(id);

         if (itemToUpdate != null)
         {
            itemToUpdate.Complete = !itemToUpdate.Complete;

            _logger.Debug($"Todo with id'{id}' successfully updated.");
            return itemToUpdate;
         }

         _logger.Error($"Can't update Todo with id'{id}', reason: does not exist!");
         return null;
      }

      public TodoModel FindById(int id)
      {
         return DataStorage.FirstOrDefault(t => t.TodoId == id);
      }

      public IEnumerable<TodoModel> GetAllTodo()
      {
         return DataStorage.Select(t => t);
      }

      private static List<TodoModel> GenerateInitialData()
      {
         _logger.Debug("Generating inital data for application.");
         return new List<TodoModel>
         {
            new TodoModel
            {
               TodoId = 1,
               TodoText = "Hello World app",
               Complete = false
            },
            new TodoModel
            {
               TodoId = 2,
               TodoText = "Goodbye Moon app",
               Complete = true
            }
         };
      }
   }
}