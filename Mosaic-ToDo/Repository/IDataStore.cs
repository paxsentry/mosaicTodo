﻿using System.Collections.Generic;
using Mosaic_ToDo.Models;

namespace Mosaic_ToDo.Repository
{
   /// <summary>
   /// Simulating the data storage
   /// </summary>
   public interface IDataStore
   {
      bool Delete(int id);

      TodoModel Update(int id);

      bool Insert(string todo);

      TodoModel FindById(int id);

      IEnumerable<TodoModel> GetAllTodo();
   }
}
