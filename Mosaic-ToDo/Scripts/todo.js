﻿$("[id^=todo]").click(
   function (e) {
      var id = this.id.slice(4);

      $.ajax({
         url: '/Todo/Update',
         type: 'POST',
         data: { todoId: id },
         success: function (data) {
            $('#todo' + id).html(data);
         },
         error: function () { }
      });
   });