﻿using System.Collections.Generic;
using Mosaic_ToDo.Models;

namespace Mosaic_ToDo.ViewModels
{
   public class TodoViewModel
   {
      public string NewTodoText { get; set; }

      public List<TodoModel> Todos { get; set; }
   }
}