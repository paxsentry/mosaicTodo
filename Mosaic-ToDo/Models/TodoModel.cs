﻿namespace Mosaic_ToDo.Models
{
   public class TodoModel
   {
      public int TodoId { get; set; }

      public bool Complete { get; set; }

      public string TodoText { get; set; }
   }
}